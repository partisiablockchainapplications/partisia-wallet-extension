# quasar
```bash
# download the quasar cli
npm i @quasar/cli@latest -g
```

# partisia-wallet (partisia-wallet-extension)

[Partisia Blockchain Wallet](https://gitlab.com/partisiaapplications/partisia-wallet-extension)

## use node v18.19.0
```bash
nvm use
```

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
npm run dev
```

### Add extension into Chrome Browser
```sh
# visit url
chrome://extensions/

# toggle developer mode and click load unpacked
# point to directory 
# ...../partisia-wallet-extension/src-bex
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
npm run build
```


### Create from quasar-cli
```bash
→ quasar create partisia-wallet-extension --branch next

? Project name (internal usage for dev) parti-wallet-extension
? Project product name (must start with letter if building mobile apps) parti-wallet
? Project description Parti Wallet
? Author PBC Applications
? Pick your CSS preprocessor: SCSS
? Check the features needed for your project: ESLint (recommended), Vuex, Axios
? Pick an ESLint preset: Prettier
? Continue to install project dependencies after the project has been created? (recommended) NPM


  cd parti-wallet-extension
  quasar dev

https://quasar.dev/quasar-cli/developing-browser-extensions/preparation
quasar mode add bex
```


```bash
# make and push releases
git tag -a v1.0.66 -m "v1.0.66"
git push origin v1.0.66
```