/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 */

// Configuration for your app
// https://v2.quasar.dev/quasar-cli/quasar-conf-js

/* eslint-env node */
const ESLintPlugin = require('eslint-webpack-plugin')
const { configure } = require('quasar/wrappers')
const archiver = require('archiver')
const path = require('path')
const manifest = require('./src-bex/manifest.json')
const fse = require('fs')

module.exports = configure(function (ctx) {
  return {
    // https://v2.quasar.dev/quasar-cli/supporting-ts
    supportTS: false,

    // https://v2.quasar.dev/quasar-cli/prefetch-feature
    // preFetch: true,

    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    // https://v2.quasar.dev/quasar-cli/boot-files
    boot: ['axios', 'landing'],

    // https://v2.quasar.dev/quasar-cli/quasar-conf-js#Property%3A-css
    css: ['app.scss'],

    // https://github.com/quasarframework/quasar/tree/dev/extras
    extras: [
      // 'ionicons-v4',
      // 'mdi-v5',
      // 'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      'line-awesome',
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      'roboto-font', // optional, you are not bound to it
      'material-icons', // optional, you are not bound to it
    ],

    // Full list of options: https://v2.quasar.dev/quasar-cli/quasar-conf-js#Property%3A-build
    build: {
      async afterBuild(cfg) {
        // after build copy the final build version into a folder `releases`
        // see outputToZip in node_modules/@quasar/app/lib/webpack/bex/plugin.bex-packager.js
        try {
          const outputToZip = async (src, dest, fileName) => {
            const filePath = path.join(dest, fileName + '.zip')
            const output = fse.createWriteStream(filePath)
            const archive = archiver('zip', {
              zlib: { level: 9 } // Sets the compression level.
            })

            archive.pipe(output)
            archive.directory(src, false)
            await archive.finalize()
          }


          const dirUnPackaged = cfg.quasarConf.bex.builder.directories.input

          const appVersion = `v${manifest.version}`
          console.log('appVersion', appVersion)
          const filePath = path.resolve(dirUnPackaged).split(path.sep)
          const filePathBase = filePath.slice(0, filePath.length - 3).join(path.sep)
          const filePathRelease = path.resolve(filePathBase, 'releases', appVersion)
          console.log('filePathRelease', filePathRelease)

          fse.rmSync(filePathRelease, { recursive: true, force: true })
          fse.mkdirSync(filePathRelease)

          await outputToZip(dirUnPackaged, filePathRelease, 'packaged')
        } catch (error) {
          console.error(error.message)
        }
        // No longer needed fixed in ver 3.1.7
        // https://github.com/quasarframework/quasar/issues/10190
        // try {
        //   // bit of a hack to move the index.html into the www folder on build of Packaged
        //   const dirUnPackaged = cfg.quasarConf.bex.builder.directories.input
        //   const { stdout } = await execa('mv', [`${dirUnPackaged}/index.html`, `${dirUnPackaged}/www/`])
        //   console.log(stdout)
        // } catch (error) {
        //   console.error(error.message)
        // }
      },
      // https://v0-17.quasar-framework.org/guide/app-pre-processors-and-webpack.html
      extendWebpack(cfg) { },
      // https://quasar.dev/quasar-cli/handling-process-env
      // extend process.env
      env: {
        // testnet
        // URL_TORUS: 'https://mpcexplorer.com/torus',
        // TORUS_NETWORK: 'ropsten',
        // TORUS_PROXY_ADDRESS: '0x4023d2a0D330bF11426B12C6144Cfb96B7fa6183',
        // TORUS_VERIFIER_GOOGLE: 'partisia-google-testnet-torus2',
        // TORUS_VERIFIER_TWITTER: 'partisia-testnet-twitter',


        // mainnet
        URL_TORUS: 'https://torus.mpcexplorer.com',
        TORUS_PROXY_ADDRESS: '0x638646503746d5456209e33a2ff5e3226d698bea',
        TORUS_VERIFIER_GOOGLE: 'partisia-google-mainnet-web',
        TORUS_VERIFIER_TWITTER: 'partisia-twitter-mainnet',

        URL_TWITTER_USER: 'https://twitter.mpcexplorer.com/twitter/user',
        URL_TWITTER_ID: 'https://twitter.mpcexplorer.com/twitter/id',
        URL_COMMUNITY_STAKE_MAINNET: 'https://mpcexplorer.com/staking/community_status',
        URL_COMMUNITY_STAKE_TESTNET: 'https://mpcexplorer.com/staking-testnet/community_status',
        ACTIVITY_CACHE: 20,
      },
      vueRouterMode: 'hash', // available values: 'hash', 'history'

      // transpile: false,

      // Add dependencies for transpiling with Babel (Array of string/regex)
      // (from node_modules, which are by default not transpiled).
      // Applies only if "transpile" is set to true.
      // transpileDependencies: [],

      // rtl: false, // https://v2.quasar.dev/options/rtl-support
      // preloadChunks: true,
      // showProgress: false,
      // gzip: true,
      // analyze: true,

      // Options below are automatically set depending on the env, set them if you want to override
      // extractCSS: false,

      // https://v2.quasar.dev/quasar-cli/handling-webpack
      // "chain" is a webpack-chain object https://github.com/neutrinojs/webpack-chain
      chainWebpack(chain) {
        chain.plugin('eslint-webpack-plugin').use(ESLintPlugin, [{ extensions: ['js', 'vue'] }])
      },
      minify: true,
      showProgress: true,

      chainWebpack(chain) {
        const nodePolyfillWebpackPlugin = require('node-polyfill-webpack-plugin')
        chain.plugin('node-polyfill').use(nodePolyfillWebpackPlugin)
        chain.plugin('eslint-webpack-plugin').use(ESLintPlugin, [{ extensions: ['js', 'vue'] }])
      },
    },

    // Full list of options: https://v2.quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
      https: false,
      port: 8080,
      open: true, // opens browser window automatically
    },

    // https://v2.quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
      config: {
        brand: {
          primary: '#61E66F',
          secondary: '#1d70ea',
        },
      },

      // iconSet: 'material-icons', // Quasar icon set
      // lang: 'en-US', // Quasar language pack

      // For special cases outside of where the auto-import stategy can have an impact
      // (like functional components as one of the examples),
      // you can manually specify Quasar components/directives to be available everywhere:
      //
      // components: [],
      // directives: [],

      // Quasar plugins
      plugins: ['Dialog', 'Loading', 'Notify', 'AppFullscreen'],
    },

    // animations: 'all', // --- includes all animations
    // https://v2.quasar.dev/options/animations
    animations: ['fadeIn', 'fadeOut', 'slideInLeft'],

    // https://v2.quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
      pwa: false,
      // manualStoreHydration: true,
      prodPort: 3000, // The default port that the production server should use
      // (gets superseded if process.env.PORT is specified at runtime)

      maxAge: 1000 * 60 * 60 * 24 * 30,
      // Tell browser when a file from the server should expire from cache (in ms)

      chainWebpackWebserver(chain) {
        chain.plugin('eslint-webpack-plugin').use(ESLintPlugin, [{ extensions: ['js', 'vue'] }])
      },

      middlewares: [
        ctx.prod ? 'compression' : '',
        'render', // keep this as last one
      ],
    },

    // https://v2.quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
      workboxPluginMode: 'GenerateSW', // 'GenerateSW' or 'InjectManifest'
      workboxOptions: {}, // only for GenerateSW

      // for the custom service worker ONLY (/src-pwa/custom-service-worker.[js|ts])
      // if using workbox in InjectManifest mode
      chainWebpackCustomSW(chain) {
        chain.plugin('eslint-webpack-plugin').use(ESLintPlugin, [{ extensions: ['js'] }])
      },

      manifest: {
        name: `parti-wallet`,
        short_name: `parti-wallet`,
        description: `Parti Wallet`,
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            src: 'icons/icon-128x128.png',
            sizes: '128x128',
            type: 'image/png',
          },
          {
            src: 'icons/icon-192x192.png',
            sizes: '192x192',
            type: 'image/png',
          },
          {
            src: 'icons/icon-256x256.png',
            sizes: '256x256',
            type: 'image/png',
          },
          {
            src: 'icons/icon-384x384.png',
            sizes: '384x384',
            type: 'image/png',
          },
          {
            src: 'icons/icon-512x512.png',
            sizes: '512x512',
            type: 'image/png',
          },
        ],
      },
    },

    // Full list of options: https://v2.quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
    cordova: {
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    // Full list of options: https://v2.quasar.dev/quasar-cli/developing-capacitor-apps/configuring-capacitor
    capacitor: {
      hideSplashscreen: true,
    },

    // Full list of options: https://v2.quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
    electron: {
      bundler: 'packager', // 'packager' or 'builder'

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',
        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        appId: 'parti-wallet-extension',
      },

      // "chain" is a webpack-chain object https://github.com/neutrinojs/webpack-chain
      chainWebpackMain(chain) {
        chain.plugin('eslint-webpack-plugin').use(ESLintPlugin, [{ extensions: ['js'] }])
      },

      // "chain" is a webpack-chain object https://github.com/neutrinojs/webpack-chain
      chainWebpackPreload(chain) {
        chain.plugin('eslint-webpack-plugin').use(ESLintPlugin, [{ extensions: ['js'] }])
      },
    },
  }
})
