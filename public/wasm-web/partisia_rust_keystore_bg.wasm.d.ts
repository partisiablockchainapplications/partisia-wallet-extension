/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function __wbg_keystore_free(a: number): void;
export function __wbg_keygen_free(a: number): void;
export function keygen_get_cipher(a: number, b: number): void;
export function keygen_get_mac(a: number, b: number): void;
export function generate_keystore(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number): number;
export function is_keystore_password(a: number, b: number, c: number, d: number): number;
export function decode_keystore(a: number, b: number, c: number, d: number, e: number): void;
export function sign(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number): void;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
