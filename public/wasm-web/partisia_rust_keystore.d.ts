/* tslint:disable */
/* eslint-disable */
/**
* @param {Uint8Array} private_key
* @param {Uint8Array} password
* @param {Uint8Array} salt
* @param {Uint8Array} iv
* @returns {KeyGen}
*/
export function generate_keystore(private_key: Uint8Array, password: Uint8Array, salt: Uint8Array, iv: Uint8Array): KeyGen;
/**
* @param {string} keystore_json
* @param {Uint8Array} password
* @returns {boolean}
*/
export function is_keystore_password(keystore_json: string, password: Uint8Array): boolean;
/**
* @param {string} keystore_json
* @param {Uint8Array} password
* @returns {Uint8Array}
*/
export function decode_keystore(keystore_json: string, password: Uint8Array): Uint8Array;
/**
* @param {string} keystore_json
* @param {Uint8Array} password
* @param {Uint8Array} digest
* @param {number | undefined} idx_hd
* @returns {Uint8Array}
*/
export function sign(keystore_json: string, password: Uint8Array, digest: Uint8Array, idx_hd?: number): Uint8Array;
/**
*/
export class KeyGen {
  free(): void;
/**
* @returns {Uint8Array}
*/
  readonly get_cipher: Uint8Array;
/**
* @returns {Uint8Array}
*/
  readonly get_mac: Uint8Array;
}
/**
*/
export class KeyStore {
  free(): void;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_keystore_free: (a: number) => void;
  readonly __wbg_keygen_free: (a: number) => void;
  readonly keygen_get_cipher: (a: number, b: number) => void;
  readonly keygen_get_mac: (a: number, b: number) => void;
  readonly generate_keystore: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number) => number;
  readonly is_keystore_password: (a: number, b: number, c: number, d: number) => number;
  readonly decode_keystore: (a: number, b: number, c: number, d: number, e: number) => void;
  readonly sign: (a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number) => void;
  readonly __wbindgen_add_to_stack_pointer: (a: number) => number;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
}

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
