import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'
import { partisiaCrypto } from 'partisia-blockchain-applications-crypto'
import VuexPersistence from 'vuex-persist'
import { Big } from 'big.js'
import { PartisiaAccount } from 'partisia-blockchain-applications-rpc'
import axios from 'axios'
import browser from 'webextension-polyfill'
import jsend from 'jsend'
import { promisify } from 'util'
import { AbiParser, JsonValueConverter, StateReader } from '@partisiablockchain/abi-client'
import { convertDec } from 'src/boot/landing'

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  const defaultStateWallet = () => {
    return {
      winType: 'normal', // popup or normal
      idleTime: 0,
      isWalletLocked: true,
      walletState: [],
      walletActiveIdx: -1,
      walletBalances: {},
      communityStakes: {},
      refreshTime: 0,
      assetTab: 'byoc',
    }
  }
  const modWalletState = {
    state: defaultStateWallet(),
    mutations: {
      RESET_STATE_WALLET(state) {
        Object.assign(state, defaultStateWallet())
      },
      WALLET_LOGGED_IN: (state, isLoggedIn) => {
        state.isWalletLocked = !isLoggedIn
      },
      IDLE_TIME: (state, now) => {
        state.idleTime = now
      },
      WALLET_LOAD: (state, payload) => {
        state.walletState = JSON.parse(JSON.stringify(payload))
      },
      WALLET_RENAME: (state, payload) => {
        const { idx, name } = payload
        state.walletState[idx].name = name
      },
      WALLET_CREATE: (state, payload) => {
        state.walletState.push(payload)
      },
      WALLET_REMOVE: (state, idx) => {
        state.walletState.splice(idx, 1)
      },
      WALLET_ACTIVE: (state, idx) => {
        state.walletActiveIdx = idx
      },
      WALLET_COMMUNITY_STAKES: (state, payload) => {
        const { address, aryCommunityStakes } = payload
        state.communityStakes[address] = { aryCommunityStakes, updatedAt: Date.now() }
      },
      WALLET_BALANCES: (state, payload) => {
        const { address, ...rest } = payload
        // console.log('payload', payload)
        state.walletBalances[address] = { ...rest, updatedAt: Date.now() }
      },
      WALLET_BALANCES_RESET: (state, address) => {
        delete state.walletBalances[address]
      },
      WALLET_REFRESH: (state, v) => {
        state.refreshTime = v
      },
      WALLET_ACTIVITY: (state, { walletIdx, aryActivity }) => {
        // Add the new activity to the old
        const activityList = (state.walletState[walletIdx]?.activity || []).sort((a, b) => a.id - b.id)
        const aryIds = activityList.map(a => a.id)
        for (const activity of aryActivity) {
          if (!aryIds.includes(activity.id)) {
            activityList.push(activity)
          }
        }

        // remove anything over last 20
        while (activityList.length > process.env.ACTIVITY_CACHE) {
          activityList.shift()
        }

        state.walletState[walletIdx].activity = JSON.parse(JSON.stringify(activityList))
      },
      TOKENS_ERC20: (state, { walletIdx, aryErc20 }) => {
        state.walletState[walletIdx].tokensErc20 = JSON.parse(JSON.stringify(aryErc20))
      },
      ERC20_TOKEN_TOGGLE: (state, { walletIdx, id }) => {
        const ary = state.walletState[walletIdx].tokensErc20Enabled || []
        const i = ary.findIndex(a => a.id === id)
        if (i === -1) { ary.push({ id, enabled: true }) } else { ary[i].enabled = !ary[i].enabled }
        state.walletState[walletIdx].tokensErc20Enabled = JSON.parse(JSON.stringify(ary))
      },
      TOKENS_ERC721: (state, { walletIdx, aryErc721 }) => {
        state.walletState[walletIdx].tokensErc721 = JSON.parse(JSON.stringify(aryErc721))
      },
      ERC721_TOKEN_TOGGLE: (state, { walletIdx, id }) => {
        const ary = state.walletState[walletIdx].tokensErc721Enabled || []
        const i = ary.findIndex(a => a.id === id)
        if (i === -1) { ary.push({ id, enabled: true }) } else { ary[i].enabled = !ary[i].enabled }
        state.walletState[walletIdx].tokensErc721Enabled = JSON.parse(JSON.stringify(ary))
      },
      RESET_TOKENS_ERC20: (state) => {
        for (const wallet of state.walletState) {
          wallet.tokensErc20 = []
          wallet.tokensErc20Enabled = []
        }
      },
      RESET_TOKENS_ERC721: (state) => {
        for (const wallet of state.walletState) {
          wallet.tokensErc721 = []
          wallet.tokensErc721Enabled = []
        }
      },
      WINDOW_TYPE: (state, payload) => {
        state.winType = payload
      },
      SET_ASSET_TAB: (state, payload) => {
        state.assetTab = payload
      },
    },
    actions: {
      resetStateWallet({ commit }) {
        commit('RESET_STATE_WALLET')
      },
      resetTokensErc20({ commit }) {
        commit('RESET_TOKENS_ERC20')
      },
      resetTokensErc721({ commit }) {
        commit('RESET_TOKENS_ERC721')
      },
      setWalletActivity(context, obj) {
        context.commit('WALLET_ACTIVITY', obj)
      },
      setWindowType(context, payload) {
        context.commit('WINDOW_TYPE', payload)
      },
      toggleTokenErc20Enabled({ commit, getters }, payload) {
        commit('ERC20_TOKEN_TOGGLE', payload)
      },
      toggleTokenErc721Enabled({ commit, getters }, payload) {
        commit('ERC721_TOKEN_TOGGLE', payload)
      },
      setAssetTab({ commit, getters }, payload) {
        commit('SET_ASSET_TAB', payload)
      },
      /* 
      // {
      //   "contract_name": "mamont",
      //   "contract_ticker_symbol": "MAM",
      //   "contract_decimals": 100,
      //   "contract_address": "028f54785c9569fdc1f7c59769c319f35903e31630",
      //   "coin": 0,
      //   "type": "PBC",
      //   "logo_url": "https://assets.unmarshal.io/tokens/partisia_028f54785c9569fdc1f7c59769c319f35903e31630.png",
      //   "verified": true,
      //   "quote_rate": 0,
      //   "quote_rate_24h": 0,
      //   "quote_pct_change_24h": 0,
      //   "balance": "7",
      //   "quote": 0
      // }
       */
      async setTokensErc20({ commit, getters }, obj) {
        const { walletIdx, aryErc20 } = obj
        for (const tkn of aryErc20) {
          try {
            // do not include BYOC as erc20, only public contracts
            if (tkn.contract_address.substr(0, 2) !== '02') continue

            const {
              contract_name,
              contract_ticker_symbol,
              contract_decimals,
              balance,
              contract_address,
              coin,
              type,
              logo_url,
              verified,
              quote_rate,
              quote_rate_24h,
              quote_pct_change_24h,
              quote
            } = tkn

            tkn.contractState = {
              name: contract_name,
              symbol: contract_ticker_symbol,
              decimals: contract_decimals,
              balance,
              balanceConvert: convertDec(balance, { decimals: contract_decimals }),
            }
            tkn.exists = true
          } catch (error) {
            // skip errors
            console.error('setTokensErc20', error.message)
            continue
          }
        }
        if (process.env.DEV) console.log('setTokensErc20', JSON.stringify(obj, null, 2))
        commit('TOKENS_ERC20', obj)
      },
      async setTokensErc721({ commit, getters }, obj) {
        const { walletIdx, aryErc721 } = obj

        const aryNFTs = []
        // get all unique NFT contracts
        const aryNftContracts = (aryErc721?.nft_assets || []).map(x => x.asset_contract).filter((val, idx, ary) => (ary.indexOf(val) === idx))
        for (const asset_contract of aryNftContracts) {
          try {
            if (asset_contract.substr(0, 2) !== '02') continue

            // get all the tokens in the collection
            const a = []
            for (const tkn of aryErc721.nft_assets.filter(x => x.asset_contract === asset_contract)) {
              const deriveUrl = (external_link, image_url) => {
                try {
                  return new URL(image_url).href
                } catch (error) {
                  return null
                }
                // try {
                //   const url = new URL(external_link.replace('{}', encodeURIComponent(image_url)))
                //   // proxy ipfs
                //   if (url.protocol === 'ipfs:') {
                //     url.protocol = 'https:'
                //     const href = url.href.endsWith('/') ? url.href.slice(0, -1) : url.href
                //     return href + '.ipfs.dweb.link'
                //   } else {
                //     return url.href
                //   }
                // } catch (error) {
                //   return null
                // }
              }
              const uri = deriveUrl(tkn.external_link, tkn.issuer_specific_data.image_url)

              a.push({
                name: tkn.asset_contract_name,
                // TODO: remove getters.accountActive.address after unmarchal update
                owner: tkn.owner || getters.walletState[walletIdx].address,
                uri,
                token_id: tkn.token_id,
              })
            }

            // tkn.contractState = getTokenErc721(contractState, getters.accountActive.address)
            aryNFTs.push({
              asset_contract,
              name: aryErc721.nft_assets.filter(x => x.asset_contract === asset_contract)?.asset_contract_name || "",
              aryTokens: a.sort((a, b) => new Big(a.token_id).minus(b.token_id)),
              exists: true
            })
          } catch (error) {
            // skip errors
            console.error('setTokensErc721', error.message)
            continue
          }
        }
        if (process.env.DEV) console.log('setTokensErc721', JSON.stringify(aryNFTs, null, 2))
        commit('TOKENS_ERC721', { walletIdx, aryErc721: aryNFTs })
      },
      walletLoggedIn(context, isLoggedIn) {
        context.commit('WALLET_LOGGED_IN', isLoggedIn)
      },
      setIdleTime(context, now) {
        context.commit('IDLE_TIME', now)
      },
      walletLoad(context, item) {
        context.commit('WALLET_LOAD', item)
      },
      walletRename(context, { idx, name }) {
        context.commit('WALLET_RENAME', { idx, name })
      },
      walletCreate(context, item) {
        context.commit('WALLET_CREATE', item)
        context.commit('WALLET_ACTIVE', context.state.walletState.length - 1)
      },
      walletRemove(context, idx) {
        context.commit('WALLET_REMOVE', idx)
        context.commit('WALLET_ACTIVE', 0)
        if (context.state.walletState.length === 0) {
          context.commit('WALLET_LOGGED_IN', false)
        }
      },
      setWallet(context, idx) {
        context.commit('WALLET_ACTIVE', idx)
      },
      setCommunityStakes(context, obj) {
        const { address, aryCommunityStakes } = obj
        context.commit('WALLET_COMMUNITY_STAKES', { address, aryCommunityStakes })
      },
      setAccountBalances(context, payload) {
        context.commit('WALLET_BALANCES', payload)
      },
      resetAccountBalances(context, address) {
        context.commit('WALLET_BALANCES_RESET', address)
      },
      refreshNow(context, v) {
        context.commit('WALLET_REFRESH', v)
      },
    },
    getters: {
      isWalletPasswordProtected: (state, getters) => {
        // For now all wallets are Password Protected
        return true
        // right now this is only Torus Wallets
        // return !getters.walletActive?.privateKey
      },
      isWalletUnlocked: (state) => {
        return !state.isWalletLocked
      },
      idleTime: (state) => {
        return state.idleTime
      },
      hasWallet: (state) => {
        return state.walletState.length ? true : false
      },
      winType: (state) => {
        return state.winType
      },
      walletState: (state) => {
        return state.walletState
      },
      walletActiveIdx: (state) => {
        return state.walletActiveIdx
      },
      walletActivity: (state, getters) => {
        return getters.walletActive?.activity || []
      },
      walletTokensErc20: (state, getters) => {
        return getters.walletActive?.tokensErc20 || []
      },
      walletTokensErc721: (state, getters) => {
        return getters.walletActive?.tokensErc721 || []
      },
      walletTokensErc20Enabled: (state, getters) => {
        return getters.walletActive?.tokensErc20Enabled || []
      },
      walletTokensErc721Enabled: (state, getters) => {
        return getters.walletActive?.tokensErc721Enabled || []
      },
      walletActive: (state, getters) => {
        // wallet is defined by defined by the bip44 HD entropy
        return getters.hasWallet ? state.walletState[state.walletActiveIdx] : null
      },
      isWalletHD: (state, getters, rootState, rootGetters) => {
        return getters.walletActive?.pub?.substr(0, 4) === 'xpub'
      },
      isWalletWatchOnly: (state, getters, rootState, rootGetters) => {
        return getters.walletActive?.pub ? false : true
      },
      accountActive: (state, getters) => {
        // account is the specific path with an HD wallet or simply the only account in a legacy wallet
        const wallet = getters.walletActive
        if (!wallet) { return null }

        if (getters.isWalletHD) {
          const idx = wallet.idx || 0
          return partisiaCrypto.wallet.walletFromXPub(wallet.pub, idx)
        } else {
          return wallet
        }
      },
      walletBalances: (state) => {
        return state.walletBalances
      },
      accountInfo: (state, getters) => {
        return state.walletBalances[getters.accountActive?.address]
      },
      communityStakes: (state, getters) => {
        return state.communityStakes[getters.accountActive?.address]?.aryCommunityStakes || []
      },
      accountBalances: (state, getters) => {
        return getters.accountInfo?.balances || []
      },
      shardId: (state, getters) => {
        const accountInfo = getters.accountInfo
        return accountInfo?.account?.shard_id ?? null
      },
      getGasBalance: (state, getters) => {
        return getters.accountBalances.map(b => new Big(b.balance).times(b.conversion.numerator).div(b.conversion.denominator)).reduce((pv, cv, idx) => pv.plus(cv), new Big(0)).toFixed(0) || '0'
      },
      refreshTime: (state, getters) => {
        return state.refreshTime
      },
      getTabAsset: (state, getters) => {
        return state.assetTab || 'byoc'
      },
    },
  }

  const urlNodes = {
    "mainnet": {
      "public-reader-node": {
        "urlBaseGlobal": { "url": "https://reader.partisiablockchain.com", "shard_id": 99 },
        "urlBaseShards":
          [
            { "url": "https://reader.partisiablockchain.com/shards/Shard0", "shard_id": 0 },
            { "url": "https://reader.partisiablockchain.com/shards/Shard1", "shard_id": 1 },
            { "url": "https://reader.partisiablockchain.com/shards/Shard2", "shard_id": 2 },
          ]
      },
      "unmarshal": {
        "urlBaseGlobal": { "url": "https://api.unmarshal.com/partisia", "shard_id": 99 },
        "urlBaseShards":
          [
            { "url": "https://api.unmarshal.com/partisiablockchain/shards/Shard0", "shard_id": 0 },
            { "url": "https://api.unmarshal.com/partisiablockchain/shards/Shard1", "shard_id": 1 },
            { "url": "https://api.unmarshal.com/partisiablockchain/shards/Shard2", "shard_id": 2 },
          ]
      },
    },

    "testnet": {
      "public-reader-node": {
        name: 'testnet',
        chainId: 'Partisia Blockchain Testnet',
        url: 'https://node1.testnet.partisiablockchain.com/shards/Shard0',
        ary_urls: {
          "urlIndexer": "https://indexer-testnet.pbdevs.com/indexer",
          "urlBaseGlobal": { "url": "https://node1.testnet.partisiablockchain.com", "shard_id": 99 },
          "urlBaseShards":
            [
              { "url": "https://node1.testnet.partisiablockchain.com/shards/Shard0", "shard_id": 0 },
              { "url": "https://node1.testnet.partisiablockchain.com/shards/Shard1", "shard_id": 1 },
              { "url": "https://node1.testnet.partisiablockchain.com/shards/Shard2", "shard_id": 2 },
            ]
        },
      },
    },
  }
  const defaultStateChain = () => {
    return {
      aryNetworkPartisia: [
        {
          name: 'mainnet',
          chainId: 'Partisia Blockchain',
          url: 'https://api.unmarshal.com/partisiablockchain/shards/Shard0',
          ary_urls: {
            "urlBaseGlobal": { "url": "https://api.unmarshal.com/partisiablockchain", "shard_id": 99 },
            "urlBaseShards":
              [
                { "url": "https://api.unmarshal.com/partisiablockchain/shards/Shard0", "shard_id": 0 },
                { "url": "https://api.unmarshal.com/partisiablockchain/shards/Shard1", "shard_id": 1 },
                { "url": "https://api.unmarshal.com/partisiablockchain/shards/Shard2", "shard_id": 2 },
              ]
          },
        },
        {
          name: 'testnet',
          chainId: 'Partisia Blockchain Testnet',
          url: 'https://api.unmarshal.com/partisiablockchain-testnet/shards/Shard0',
          ary_urls: {
            "urlBaseGlobal": { "url": "https://api.unmarshal.com/partisiablockchain-testnet", "shard_id": 99 },
            "urlBaseShards":
              [
                { "url": "https://api.unmarshal.com/partisiablockchain-testnet/shards/Shard0", "shard_id": 0 },
                { "url": "https://api.unmarshal.com/partisiablockchain-testnet/shards/Shard1", "shard_id": 1 },
                { "url": "https://api.unmarshal.com/partisiablockchain-testnet/shards/Shard2", "shard_id": 2 },
              ]
          }
        },
        // {
        //   name: 'mainnet',
        //   chainId: 'Partisia Blockchain',
        //   url: 'https://reader.partisiablockchain.com/shards/Shard0',
        //   ary_urls: {
        //     "urlIndexer": "https://indexer.pbdevs.com/indexer",
        //     "urlBaseGlobal": { "url": "https://reader.partisiablockchain.com", "shard_id": 99 },
        //     "urlBaseShards":
        //       [
        //         { "url": "https://reader.partisiablockchain.com/shards/Shard0", "shard_id": 0 },
        //         { "url": "https://reader.partisiablockchain.com/shards/Shard1", "shard_id": 1 },
        //         { "url": "https://reader.partisiablockchain.com/shards/Shard2", "shard_id": 2 },
        //       ]
        //   }
        // },
        // {
        //   name: 'testnet',
        //   chainId: 'Partisia Blockchain Testnet',
        //   url: 'https://node1.testnet.partisiablockchain.com/shards/Shard0',
        //   ary_urls: {
        //     "urlIndexer": "https://indexer-testnet.pbdevs.com/indexer",
        //     "urlBaseGlobal": { "url": "https://node1.testnet.partisiablockchain.com", "shard_id": 99 },
        //     "urlBaseShards":
        //       [
        //         { "url": "https://node1.testnet.partisiablockchain.com/shards/Shard0", "shard_id": 0 },
        //         { "url": "https://node1.testnet.partisiablockchain.com/shards/Shard1", "shard_id": 1 },
        //         { "url": "https://node1.testnet.partisiablockchain.com/shards/Shard2", "shard_id": 2 },
        //       ]
        //   }
        // },
      ],
      networkPartisiaIdx: 0,
      networkShardIdx: 0,
      networkCustom: { url: '', chainId: '' },
    }
  }
  const modChain = {
    state: defaultStateChain(),
    mutations: {
      RESET_STATE_CHAIN(state) {
        Object.assign(state, defaultStateChain())
      },
      RESET_NETWORK(state) {
        Object.assign(state.aryNetworkPartisia, defaultStateChain().aryNetworkPartisia)
      },
      NETWORK_SHARD: (state, idx) => {
        state.networkShardIdx = idx
      },
      NETWORK_CHANGE: (state, idx) => {
        state.networkPartisiaIdx = idx
      },
      NETWORK_CUSTOM: (state, { url, chainId }) => {
        state.networkCustom = { url, chainId }
      },
      SET_ACTIVE_URL: (state, url) => {
        const network = state.aryNetworkPartisia[state.networkPartisiaIdx]
        // console.log('setting network.url from to ', { network: network.url, url })
        // console.log('network', JSON.stringify(network, null, 2))
        network.url = url
      },
    },
    actions: {
      resetStateChain({ commit }) {
        commit('RESET_STATE_CHAIN')
      },
      resetNetworkPartisia({ commit }) {
        commit('RESET_NETWORK')
      },
      networkChange(context, idx) {
        if (context.state.networkPartisiaIdx !== idx) {
          context.commit('NETWORK_CHANGE', idx)
          context.commit('NETWORK_SHARD', 0)
        }
      },
      networkShard(context, idx) {
        if (context.state.networkShardIdx !== idx) {
          context.commit('NETWORK_SHARD', idx)
        }
      },
      setNetworkCustom(context, { url, chainId }) {
        context.commit('NETWORK_CHANGE', -1)
        context.commit('NETWORK_SHARD', 0)
        context.commit('NETWORK_CUSTOM', { url, chainId })
      },
      setActiveNetworkUrl(context, url) {
        context.commit('SET_ACTIVE_URL', url)
      },
    },
    getters: {
      aryNetworkPartisia: (state) => {
        return state.aryNetworkPartisia
      },
      networkCustom: (state) => {
        return state.networkCustom
      },
      networkActive: (state) => {
        if (state.networkPartisiaIdx === -1) {
          return { ...state.networkCustom, name: `${state.networkCustom.url.substr(0, 12)}...`, shard: '' }
        } else {
          const network = state.aryNetworkPartisia[state.networkPartisiaIdx]
          const shard = (network.shards || []).length ? network.shards[state.networkShardIdx] : ''
          return { ...network, shard }
        }
      },
      networkUrl: (state, getters) => {
        const networkActive = getters.networkActive
        let url = networkActive.url

        return url
      },
      isTestnet: (state, getters) => {
        const networkActive = getters.networkActive
        return networkActive.name === 'testnet'
      },
      byocCoins: (state, getters) => {
        const map = getters.isTestnet
          ? [
            { symbol: 'ETH GOERLI', decimals: 18, token_name: 'Ether', address: '01dce90b5a0b6eb598dd6b4250f0f5924eb4a4a818' },
            { symbol: 'TEST COIN', decimals: 0, token_name: 'Test Coin', address: '01f3cc99688e6141355c53752418230211facf063c' },
          ]
          : [
            { symbol: 'ETH', decimals: 18, token_name: 'Ether', address: '014a6d0fd09fe2e6853a76caedcb46646ab7ee69d6' },
            { symbol: 'USDC', decimals: 6, token_name: 'USD Coin', address: '01e0dbf1ce62c4ebd76fa8aa81f3630e0e84001206' },
            { symbol: 'BNB', decimals: 18, token_name: 'Binance Coin', address: '0137f4da8ad6a9a5305383953d4b3a9c7859c08bea' },
            { symbol: 'MATIC', decimals: 18, token_name: 'Matic Token', address: '01d9f82e98a22b319aa371e752f3e0d85bd96c9545' },
            { symbol: 'USDT', decimals: 6, token_name: 'Tether USD', address: '011150c3a2779309ff52e86c139ff58265a93fafd4' },
          ]
        return map
      },
    }
  }

  const defaultStateOptions = () => {
    return {
      isFullScreen: false,
      mnemonicWords: 24,  //[12, 15, 18, 21, 24]
      openFullScreen: false,
      alwaysKeepEncrypted: true,
      lockTimeout: 10,
      transactionValidTimeout: 60,
      blockExplorerAccount: 'https://browser.partisiablockchain.com/accounts/{0}',
      blockExplorerAccountTestnet: 'https://browser.testnet.partisiablockchain.com/accounts/{0}',
      blockExplorerTransaction: 'https://browser.partisiablockchain.com/transactions/{0}',
      blockExplorerTransactionTestnet: 'https://browser.testnet.partisiablockchain.com/transactions/{0}',
      blockExplorerContract: 'https://browser.partisiablockchain.com/contracts/{0}',
      blockExplorerContractTestnet: 'https://browser.testnet.partisiablockchain.com/contracts/{0}',
      preferPopout: false,
    }
  }
  const modOptions = {
    state: defaultStateOptions(),
    getters: {
      getOptions: (state) => {
        return state
      },
    },
    mutations: {
      RESET_STATE_OPTIONS(state) {
        Object.assign(state, defaultStateOptions())
      },
      SET_STATE_OPTIONS(state, opts) {
        Object.assign(state, opts)
      },
      SET_MNEMONIC_WORD_LENGTH(state, v) {
        state.mnemonicWords = v
      },
    },
    actions: {
      resetStateOptions({ commit }) {
        commit('RESET_STATE_OPTIONS')
      },
      setOptions({ commit }, opts) {
        commit('SET_STATE_OPTIONS', opts)
      },
      setMnemonicWordLength({ commit }, v) {
        commit('SET_MNEMONIC_WORD_LENGTH', v)
      },
    }
  }

  const defaultStateRoutes = () => {
    return {
      route: null,
    }
  }
  const modRoutes = {
    state: defaultStateRoutes(),
    mutations: {
      RESET_STATE_ROUTES(state) {
        Object.assign(state, defaultStateRoutes())
      },
      ROUTE_CHANGE: (state, route) => {
        state.route = route
      },
    },
    actions: {
      resetStateRoutes({ commit }) {
        commit('RESET_STATE_ROUTES')
      },
      routeChange(context, route) {
        if (context.state.route !== route) {
          context.commit('ROUTE_CHANGE', route)
        }
      },
    },
    getters: {
      getRoute: (state) => {
        return state.route
      },
    },
  }

  const keyStorage = 'vuex'
  const persistedState = new VuexPersistence({
    key: keyStorage,
    modules: ['route', 'chain', 'wallet', 'options', 'wasm'],
    asyncStorage: true,
    storage: {
      getItem: async key => {
        // this is for legacy migration; check if key is in localStorage
        if (window.localStorage.hasOwnProperty(keyStorage)) {
          if (process.env.DEV) console.log('set from local storage')
          const localState = window.localStorage.getItem(keyStorage)
          await browser.storage.local.set({ [keyStorage]: localState })
          window.localStorage.removeItem(keyStorage)
        }

        const item = await browser.storage.local.get([key])
        if (item && item.hasOwnProperty(key))
          return JSON.parse(item[key])
        else
          return null
      },
      setItem: async (key, value) => {
        await browser.storage.local.set({ [key]: JSON.stringify(value) })
      },
      removeItem: async key => {
        await browser.storage.local.remove(key)
      },
      clear: async () => {
        await browser.storage.local.remove(keyStorage)
      },
    }
  })

  const Store = createStore({
    plugins: [persistedState.plugin],
    modules: {
      route: modRoutes,
      chain: modChain,
      wallet: modWalletState,
      options: modOptions,
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING,
  })

  return Store
})
