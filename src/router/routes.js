const routes = [
  // { path: '/torus/:oauth/:testnet', component: () => import('src/pages/Torus.vue') },
  { path: '/options', component: () => import('pages/OptionsPage.vue') },
  { path: '/popup', component: () => import('pages/PopupPage.vue') },
  { path: '/full', component: () => import('pages/FullPage.vue') },
  { path: '/confirm-connection', component: () => import('pages/ConfirmConnection.vue') },
  { path: '/confirm-sign', component: () => import('pages/ConfirmSign.vue') },
  { path: '/confirm-key', component: () => import('pages/ConfirmKey.vue') },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
]

export default routes
