import { PDFDocument, StandardFonts } from "pdf-lib";
import { Notify, exportFile } from 'quasar'

export async function saveTextAsPDF(text) {
    const pdfDoc = await PDFDocument.create();
    const page = pdfDoc.addPage();
    const helveticaFont = await pdfDoc.embedFont(StandardFonts.Helvetica);

    const { width, height } = page.getSize();
    const fontSize = 12;
    const lineHeight = 14;
    const margin = 50;
    const textWidth = width - 2 * margin;

    const textLines = text.split('\n');

    let y = height - margin;

    for (const line of textLines) {
        const textWidth = helveticaFont.widthOfTextAtSize(line, fontSize);
        const textHeight = fontSize;

        if (y - lineHeight < margin) {
            page = pdfDoc.addPage();
            y = height - margin;
        }

        page.drawText(line, {
            x: margin,
            y,
            size: fontSize,
            font: helveticaFont,
        });

        y -= lineHeight;
    }

    const pdfBytes = await pdfDoc.save();

    // Save the PDF file
    const status = exportFile('mnemonic_phrase.pdf', pdfBytes, {
        mimeType: 'application/pdf'
    })

    if (status === true) {
        // browser allowed it
    }
    else {
        // browser denied it
        console.error('Error: ' + status)
        Notify.create({ type: 'negative', message: 'Unable to download PDF file' })
    }
}

// Usage example
// const text = 'This is some sample text.\nYou can add multiple lines.';
// saveTextAsPDF(text);