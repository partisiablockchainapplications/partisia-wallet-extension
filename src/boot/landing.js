import { boot } from 'quasar/wrappers'
import { partisiaCrypto } from 'partisia-blockchain-applications-crypto'
import { PartisiaAccount, PartisiaRpc } from 'partisia-blockchain-applications-rpc'
import browser from 'webextension-polyfill'
import assert from 'assert'
import { Big } from 'big.js'
import axios from 'axios'
import init, { decode_keystore } from "partisia-rust-keystore"
import { AppFullscreen, Dialog, exportFile } from 'quasar'
import { AbiParser, FnKinds, JsonRpcConverter, JsonValueConverter, RpcReader } from "@partisiablockchain/abi-client"
import { calcTotalMPC } from "partisia-token-calculation"

export default boot(async ({ app, urlPath, redirect, store }) => {
  await init()

  // fetchBalance(store)
  app.config.globalProperties.fnPreviewText = fnPreviewText
  app.config.globalProperties.fnTimeAgo = timeAgo
  app.config.globalProperties.$dev = process.env.DEV
  // app.config.globalProperties.fnGetBalance = fnGetBalance
  // app.provide('fnGetBalance', fnGetBalance)
  // if (urlPath === '/') {
  //   redirect({ path: '/popup' })
  //   return
  // }
  const win = await browser.windows.getCurrent()
  // this is causing problems so fo now just default to false
  const preferPopout = false // await store.getters.getOptions.preferPopout
  // if ((preferPopout) && win.type !== 'popup') {
  // if ((preferPopout || store.getters.walletState.length === 0) && win.type !== 'popup') {
  //   await windowPopup()
  //   window.close()
  // }

  await store.dispatch('setWindowType', win.type)
})

export const windowedProps = async () => {
  const win = await browser.windows.getCurrent()
  const isWin = (await browser.runtime.getPlatformInfo()).os === 'win'
  const isMac = (await browser.runtime.getPlatformInfo()).os === 'mac'
  const CONFIRM_WINDOW_WIDTH = 360
  const CONFIRM_WINDOW_HEIGHT = 600
  const top = Math.round(win.top + win.height / 2 - CONFIRM_WINDOW_HEIGHT / 2)
  const left = Math.round(win.left + win.width / 2 - CONFIRM_WINDOW_WIDTH / 2)

  const offsetX = isWin ? 16 : 0
  const offsetY = isWin ? 39 : isMac ? 30 : 0

  return {
    focused: true,
    state: 'normal',
    type: 'popup',
    width: CONFIRM_WINDOW_WIDTH + offsetX,
    height: CONFIRM_WINDOW_HEIGHT + offsetY,
    top: Math.max(top, 20),
    left: Math.max(left, 20),
  }
}

export function triggerWalletRefresh(store, opts = {}) {
  fetchBalance(store, opts)
  fetchTokensErc20(store, opts)
  fetchTokensErc721(store, opts)
}
const fnPreviewText = (trx) => {
  const TokenInvocation = partisiaCrypto.transaction.CONSTANTS.TokenInvocation
  switch (trx.payload.invocationType) {
    case TokenInvocation.TRANSFER:
      return `Send <b>${trx.payload.amount}</b> tokens (<b>${trx.payload.symbol}</b>) to <span style="font-size: 0.7rem;font-weight:bold;">${trx.payload.to}</span>`
    case TokenInvocation.MINT:
      return `Mint <b>${trx.payload.amount}</b> more tokens for symbol (<b>${trx.payload.symbol}</b>)`
    case TokenInvocation.DESTROY:
      return `Burn <b>${trx.payload.amount}</b> tokens for symbol (<b>${trx.payload.symbol}</b>)`
    case TokenInvocation.PAUSE:
      return `PAUSE all transfers for token (<b>${trx.payload.symbol}</b>)`
    case TokenInvocation.UNPAUSE:
      return `RESUME transfers for token (<b>${trx.payload.symbol}</b>)`
    case TokenInvocation.CREATE:
      return `Create a new token with a total supply of <b>${trx.payload.amount}</b> and a symbol of (<b>${trx.payload.symbol}</b>)`
    case TokenInvocation.SPLIT:
      return `Split token (<b>${trx.payload.symbol}</b>) by a factor of <b>${trx.payload.factor}</b>`
  }
  return ''
}
function timeAgo(dateParam) {
  if (!dateParam) {
    return null;
  }

  const date = typeof dateParam === 'object' ? dateParam : new Date(dateParam);
  const DAY_IN_MS = 86400000; // 24 * 60 * 60 * 1000
  const today = new Date();
  const yesterday = new Date(today - DAY_IN_MS);
  const seconds = Math.round((today - date) / 1000);
  const minutes = Math.round(seconds / 60);
  const isToday = today.toDateString() === date.toDateString();
  const isYesterday = yesterday.toDateString() === date.toDateString();
  const isThisYear = today.getFullYear() === date.getFullYear();


  if (seconds < 5) {
    return 'now';
  } else if (seconds < 60) {
    return `${seconds} seconds ago`;
  } else if (seconds < 90) {
    return 'about a minute ago';
  } else if (minutes < 60) {
    return `${minutes} minutes ago`;
  } else if (isToday) {
    return getFormattedDate(date, 'Today'); // Today at 10:20
  } else if (isYesterday) {
    return getFormattedDate(date, 'Yesterday'); // Yesterday at 10:20
  } else if (isThisYear) {
    return getFormattedDate(date, false, true); // 10. January at 10:20
  }

  return getFormattedDate(date); // 10. January 2017. at 10:20
}

const MONTH_NAMES = [
  'January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December'
];


function getFormattedDate(date, prefomattedDate = false, hideYear = false) {
  const day = date.getDate();
  const month = MONTH_NAMES[date.getMonth()];
  const year = date.getFullYear();
  const hours = date.getHours();
  let minutes = date.getMinutes();

  if (minutes < 10) {
    // Adding leading zero to minutes
    minutes = `0${minutes}`;
  }

  if (prefomattedDate) {
    // Today at 10:20
    // Yesterday at 10:20
    return `${prefomattedDate} at ${hours}:${minutes}`;
  }

  if (hideYear) {
    // 10. January at 10:20
    return `${day}. ${month} at ${hours}:${minutes}`;
  }

  // 10. January 2017. at 10:20
  return `${day}. ${month} ${year}. at ${hours}:${minutes}`;
}

export const windowPopup = async () => {
  const winProps = await windowedProps()
  const newWin = await browser.windows.create({
    ...winProps,
    url: browser.runtime.getURL(`www/index.html#/popup`),
  })
  const tabId = newWin?.tabs[0]?.id
  assert(tabId)
  // attempt to prevent fullscreen window
  await AppFullscreen.exit()
  return tabId
}

// this will be 60 seconds
export const broadcastTransactionPoller = async (trxHash, rpc, num_iter = 30, interval_sleep = 2000) => {
  let intCounter = 0
  while (++intCounter < num_iter) {
    try {
      if (process.env.DEV) console.log('intCounter', intCounter)
      const resTx = await rpc.getTransaction(trxHash)
      if (resTx.finalized) {
        break
      }
    } catch (error) {
      console.error(error.message)
    } finally {
      const sleep = (ms) => {
        return new Promise((resolve) => setTimeout(resolve, ms))
      }
      await sleep(interval_sleep)
    }
  }
  return intCounter < num_iter
}
export const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms))
}
export const getCoinBalances = async (rpcAccount, address, shardId) => {
  const account = await rpcAccount.getAccount(address, shardId)
  if (account) {
    const { coins } = await rpcAccount.fetchCoins()
    const balances = []
    for (let idx = 0; idx < account.data.accountCoins.length; idx++) {
      const coin = account.data.accountCoins[idx];
      // const conversion = new Big(coins[0].conversionRate.numerator).div(Number(coins[0].conversionRate.denominator)).toFixed()
      balances.push({
        balance: coin.balance,
        symbol: coins[0].symbol,
        conversion: {
          numerator: coins[idx].conversionRate.numerator,
          denominator: coins[idx].conversionRate.denominator,
        },
      })
    }
    return { account, coins, balances }
  }
  return null
}

/* This is no longer needed */
export const fetchActivity = async (store, opts = {}) => {
  // const { isRepeat = true, params = { n: 20, asc: true, id: 0, skip_payload: true } } = opts
  // const account = store.getters.accountActive
  // if (account) {
  //   try {
  //     const address = store.getters.accountActive.address

  //     const qs = new URLSearchParams(params).toString()
  //     if (process.env.DEV) console.log('qs', qs)
  //     const aryTrx = (
  //       await axios({
  //         method: 'GET',
  //         baseURL: `${store.getters.networkActive.ary_urls.urlIndexer}/transactions/address/${address}?${qs}`,
  //       })
  //     ).data
  //     if (process.env.DEV) console.log('trxs', aryTrx)


  //     params.id = aryTrx[aryTrx.length - 1]?.id || params.id
  //     await store.dispatch('setWalletActivity', { walletIdx: store.getters.walletActiveIdx, aryActivity: aryTrx })
  //   } catch (error) {
  //     console.error('error fetchActivity', error.message)
  //   } finally {
  //     // For resets
  //     if (store.getters.accountActive !== null && isRepeat) {
  //       await sleep(60 * 1000)
  //       fetchActivity(store, { params })
  //     }
  //   }
  // }
}

/* This is no longer needed */
export const fetchCommunityStake = async (store, opts = {}) => {
  // const { isRepeat = true } = opts
  // const accountInfo = store.getters.accountActive
  // if (accountInfo) {
  //   try {
  //     const address = store.getters.accountActive.address
  //     const { db_delegated_stakes_from_others } = (await axios({
  //       method: 'GET',
  //       baseURL: store.getters.isTestnet ? process.env.URL_COMMUNITY_STAKE_TESTNET : process.env.URL_COMMUNITY_STAKE_MAINNET,
  //     })).data

  //     const aryCommunityStakes = db_delegated_stakes_from_others.filter(d => d.address_to === address)
  //     await store.dispatch('setCommunityStakes', { address, aryCommunityStakes })
  //   } catch (error) {
  //     console.error('error fetchCommunityStake', error.message)
  //   } finally {
  //     // For resets
  //     if (store.getters.accountActive !== null && isRepeat) {
  //       await sleep(60 * 1000)
  //       fetchCommunityStake(store)
  //     }
  //   }
  // }
}
// deprecated
export const fetchTokensErc20 = async (store, opts = {}) => {
  const { isRepeat = true } = opts
  const accountInfo = store.getters.accountActive
  if (accountInfo) {
    try {
      const address = store.getters.accountActive.address
      const walletIdx = store.getters.walletActiveIdx
      const base = store.getters.isTestnet ? 'https://api.unmarshal.com/v1/partisiablockchain-testnet' : 'https://api.unmarshal.com/v1/partisia'

      const aryErc20 = (await axios({
        method: 'GET',
        // baseURL: `${store.getters.networkActive.ary_urls.urlIndexer}/contract/${address}/1`,
        baseURL: `${base}/address/${address}/assets?verified=false&includeLowVolume=true&auth_key=qMZNSjEq8hLderpxWfgIB28qPkfotSm3hbpR7Zjh`
      })).data
      if (process.env.DEV) console.log('aryErc20', JSON.stringify(aryErc20, null, 2))
      await store.dispatch('setTokensErc20', { walletIdx, aryErc20 })
    } catch (error) {
      console.error('error fetchTokensErc20', error.message)
    } finally {
      // For resets
      if (store.getters.accountActive !== null && isRepeat) {
        await sleep(60 * 1000)
        fetchTokensErc20(store, opts)
      }
    }
  } else {
    store.dispatch('resetTokensErc20')
  }
}
// deprecated
export const fetchTokensErc721 = async (store, opts = {}) => {
  const { isRepeat = true } = opts
  const accountInfo = store.getters.accountActive
  if (accountInfo) {
    try {
      const address = store.getters.accountActive.address
      const walletIdx = store.getters.walletActiveIdx
      const base = store.getters.isTestnet ? 'https://api.unmarshal.com/v3/partisiablockchain-testnet' : 'https://api.unmarshal.com/v3/partisia'

      const aryErc721 = (await axios({
        method: 'GET',
        // baseURL: `${store.getters.networkActive.ary_urls.urlIndexer}/contract/${address}/2`,
        // baseURL: `${store.getters.networkActive.ary_urls.urlIndexer}/contract/${address}/2`,
        baseURL: `${base}/address/${address}/nft-assets?auth_key=qMZNSjEq8hLderpxWfgIB28qPkfotSm3hbpR7Zjh`
      })).data

      await store.dispatch('setTokensErc721', { walletIdx, aryErc721 })
    } catch (error) {
      console.error('error fetchTokensErc721', error.message)
    } finally {
      // For resets
      if (store.getters.accountActive !== null && isRepeat) {
        await sleep(60 * 1000)
        fetchTokensErc721(store, opts)
      }
    }
  } else {
    store.dispatch('resetTokensErc721')
  }
}

export const fetchBalance = async (store, opts = {}) => {
  const { isRepeat = true } = opts
  const accountActive = store.getters.accountActive
  const address = accountActive?.address
  try {
    const aryUrls = store.getters.networkActive.ary_urls
    const rpcAccount = PartisiaAccount(aryUrls, { auth_key: 'qMZNSjEq8hLderpxWfgIB28qPkfotSm3hbpR7Zjh' })
    if (address) {
      const accountInfo = await getCoinBalances(rpcAccount, address, store.getters.shardId)
      if (process.env.DEV) console.log('fetchBalance %s %s', address, !!accountInfo)
      if (accountInfo) {
        if (process.env.DEV) console.log('accountInfo', JSON.stringify(accountInfo, null, 2))

        // update for the url that the account exists in
        store.dispatch('setActiveNetworkUrl', rpcAccount.getShardUrl(accountInfo.account.shard_id))
        store.dispatch('setAccountBalances', {
          balances: accountInfo.balances,
          tokens: accountInfo.coins,
          address,
          account: accountInfo.account,
        })
      } else {
        store.dispatch('resetAccountBalances', address)
      }
    }
    // rpc.updateBaseUrl(store.getters.networkUrl)
  } catch (error) {
    // if it is no isRepeat then remove the balances because this is bad config
    if (!isRepeat) {
      store.dispatch('resetAccountBalances', address)
    }
    console.error('error fetchBalance', error.message)
  } finally {
    // For resets
    if (store.getters.accountActive === null) {
      return
    }

    // We are no longer displaying activity
    // also refresh activity for the timeago text
    // await store.dispatch('setWalletActivity', { walletIdx: store.getters.walletActiveIdx, aryActivity: store.getters.walletActivity })
    if (isRepeat) {
      await sleep(60 * 1000)
      fetchBalance(store, opts)
    }
  }
}
const addressType = (address) => {
  const byte = parseInt(address.substr(0, 2), 16)
  switch (byte) {
    case 0:
      return 'Address'
    case 1:
      return 'System Contract'
    case 2:
      return 'Public Contract'
    case 3:
      return 'ZK Contract'
    default:
      // Really this is unknown but display contract
      return 'Contract'
  }
}

export const getInvocationType = (headerContract) => {
  return partisiaCrypto.structs.SYSTEM_CONTRACTS.find(c => c.contract === headerContract)?.name || addressType(headerContract)
}

export const convertDec = (amt, opts = {}) => {
  var { conversion = 1, decimals = 0, convertFromDecimals = true, removeTrailingZeros = true } = opts
  if (convertFromDecimals) conversion = `1e${decimals}`

  // seems like this doesnt need to be adjusted for the 4 decimals places
  // https://stackoverflow.com/questions/721304/insert-commas-into-number-string
  let amtStr = new Big(amt).div(conversion).toFixed(Number(decimals))
  if (amtStr.indexOf('.') !== -1) {
    while (amtStr.slice(-1) === '0' && removeTrailingZeros) {
      amtStr = amtStr.slice(0, -1)
    }
    if (amtStr.slice(-1) === '.') {
      amtStr = amtStr.slice(0, -1)
    }
  }
  if (amtStr.indexOf('.') !== -1) {
    return amtStr.replace(/\d{1,3}(?=(\d{3})+(?=\.))/g, "$&,")
  } else {
    return amtStr.replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&,")
  }
}

export const publicContractActionToObject = (abi, payload) => {
  const abiBuf = typeof abi === 'string' ? Buffer.from(abi, 'hex') : abi
  const payloadBuf = typeof payload === 'string' ? Buffer.from(payload, 'hex') : payload

  const fileAbi = new AbiParser(abiBuf).parseAbi()
  const rpc = new RpcReader(payloadBuf, fileAbi, FnKinds.action).readRpc()
  return JsonRpcConverter.toJson(rpc)
}

export const promptPassword = async (keystore) => {
  return new Promise((resolve, reject) => {
    Dialog.create({
      // title:'Unlock Wallet',
      seamless: true,
      message: 'Please Enter Wallet Password',
      position: 'bottom',
      prompt: {
        model: '',
        isValid: (val) => val.length > 0,
        type: 'password',
        outlined: true,
        standout: true,
        square: true,
        color: 'secondary',
      },
      color: 'secondary',
      cancel: true,
      persistent: true,
    }).onOk(async (walletPassword) => {
      try {
        const wif = decode_keystore(typeof keystore === 'string' ? keystore : JSON.stringify(keystore), Buffer.from(walletPassword))
        resolve(Buffer.from(wif).toString('hex'))
      } catch (msg) {
        reject({ message: msg })
      }
    }).onCancel(() => {
      reject({ message: 'user cancel' })
    }).onDismiss(() => {
      reject({ message: 'user cancel' })
    })
  })
}

export const mpcTokenBalance = (store) => {
  // return new Big(store.getters.accountInfo?.account?.data?.mpcTokens || 0).div(10000).plus(mpcVested(store)).toFixed(4)
  return new Big(calcTotalMPC(store.getters.accountInfo?.account?.data)).div(10000)
}

export const mpcDelegatedFromOthers = (store) => {
  const ary = store.getters.accountInfo?.account?.data?.delegatedStakesFromOthers || []
  const delegatedStakesFromOthers = ary.reduce((pv, cv, idx) => pv.plus(cv.value.acceptedDelegatedStakes), new Big(0))
  return new Big(delegatedStakesFromOthers).div(10000).plus(mpcVested(store)).toFixed(4)
}

export const mpcStaked = (store) => {
  const staked = store.getters.accountInfo?.account?.data?.stakedTokens || 0
  return new Big(staked).div(10000).toFixed(4)
}

export const mpcVested = (store) => {
  const ary = store.getters.accountInfo?.account?.data?.vestingAccounts || []
  const amt = ary.reduce((pv, cv, idx) => pv.plus(cv.tokens), new Big(0)) || 0
  return amt.div(10000).toFixed(4)
}

export const mpcAvailable = (store) => {
  return new Big(store.getters.accountInfo?.account?.data?.mpcTokens || 0).div(10000).toFixed(4)
}

export const TOKEN_TYPE = {
  MPC: 'MPC',
  ERC20: 'ERC20',
  BYOC: 'BYOC',
  ERC721: 'ERC721',
}
export const exportData = (content, name, mimeType) => {
  const status = exportFile(name, content, {
    mimeType,
  })
  return status
}

export const formatBytes = (bytes) => {
  const kb = new Big(bytes || 0).div(1024).toFixed(2)
  if (kb.indexOf('.') <= 3) {
    return `${kb} KB`
  }
  const mb = new Big(kb).div(1024).toFixed(2)
  if (mb.indexOf('.') <= 3) {
    return `${mb} MB`
  }
  const gb = new Big(mb).div(1024).toFixed(2)
  return `${gb} GB`
}