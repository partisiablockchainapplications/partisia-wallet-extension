import { isValidEventResponseKey } from './dom/valid-event'
import assert from 'assert'
import jsend from 'jsend'

// Hooks added here have a bridge allowing communication between the BEX Content Script and the Quasar Application.
// More info: https://quasar.dev/quasar-cli/developing-browser-extensions/content-hooks
export default function attachContentHooks(bridge) {
  // const port = browser.runtime.connect({ name: `partisia` });
  // port.onDisconnect.addListener(() => {
  //   console.log('extension has disconnected')
  // })

  bridge.on('on.partisia.wallet.tab.relay', async event => {
    assert(isValidEventResponseKey(event.eventResponseKey, 'on.partisia.wallet.tab.relay') === true)
    // event.data should be encrypted with random key generated on client side
    await bridge.send('on.dom.relay.tab', event.data)
    if (process.env.DEV) console.log('on.partisia.wallet.tab.relay', JSON.stringify(event, null, 2))
    bridge.send('close.confirm.window', { tabId: event.data.data.tabId })
  })

  bridge.on('on.confirm.window', async event => {
    assert(isValidEventResponseKey(event.eventResponseKey, 'on.confirm.window') === true)
    // data in confirmWinEvent will be encrypted within a crypto box of the sdk
    const confirmWinEvent = await bridge.send('open.confirm.window', event.data)
    bridge.send(event.eventResponseKey, confirmWinEvent.data)
  })

}
