import assert from "assert";

export const isValidEventResponseKey = (eventResponseKey, prefix) => {
    try {
        // on.dom.relay.tab.ba3a6343-f85d-4925-9c9d-d30cd63923a5.result
        assert(typeof eventResponseKey === 'string' && eventResponseKey.length === prefix.length + 1 + 36 + 7)
        assert(eventResponseKey.substr(0, prefix.length + 1) === `${prefix}.`)
        assert(eventResponseKey.substr(prefix.length + 1 + 36) === '.result')
        const uuid = eventResponseKey.substr(prefix.length + 1, 36)
        const regexExp = /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/gi;
        assert(regexExp.test(uuid))

        return true
    } catch (error) {
        return false
    }
}