import browser from 'webextension-polyfill'
import crypto from 'crypto'
import { partisiaCrypto } from 'partisia-blockchain-applications-crypto'
import jsend from 'jsend'
import * as bson from 'bson'
import assert from 'assert'
import { windowPopup, windowedProps } from 'src/boot/landing'

// Hooks added here have a bridge allowing communication between the BEX Background Script and the BEX Content Script.
// Note: Events sent from this background script using `bridge.send` can be `listen`'d for by all client BEX bridges for this BEX

// More info: https://quasar.dev/quasar-cli/developing-browser-extensions/background-hooks
export default function attachBackgroundHooks(bridge, allActiveConnections) {
  bridge.on('storage.get', async event => {
    const payload = event.data
    if (payload.key === null) {
      await browser.storage.local.get(null)
      const result = []

      // Group the items up into an array to take advantage of the bridge's chunk splitting.
      for (const key in r) {
        result.push({ key, value: r[key] })
      }
      bridge.send(event.eventResponseKey, result)
    } else {
      const r = await browser.storage.local.get([payload.key])
      bridge.send(event.eventResponseKey, r[payload.key])
    }
  })

  bridge.on('storage.set', async event => {
    const payload = event.data
    await browser.storage.local.set({ [payload.key]: payload.data })
    bridge.send(event.eventResponseKey, payload.data)
  })

  bridge.on('storage.remove', async event => {
    const payload = event.data
    await browser.storage.local.remove(payload.key)
    bridge.send(event.eventResponseKey, payload.data)
  })

  bridge.on('storage.clear', async event => {
    await browser.storage.local.clear()
    bridge.send(event.eventResponseKey)
  })

  bridge.on('wallet.state', async event => {
    // store the encrypted mnemonic into storage
    const { wallets } = await browser.storage.local.get(['wallets'])

    const d = wallets || []

    bridge.send(event.eventResponseKey, d.map(x => x))
  })

  bridge.on('wallet.set', async event => {
    await browser.storage.local.set({ ['wallets']: event.data })
    bridge.send(event.eventResponseKey, jsend.success(true))
  })

  bridge.on('wallet.create', async event => {
    // store the encrypted mnemonic into storage
    const { name, keystore, pub, address, privateKey, torusType, torusName } = event.data
    const { wallets } = await browser.storage.local.get(['wallets'])

    const d = wallets || []
    // check the address does not already exists
    if (d.map(x => x.address).includes(address)) {
      bridge.send(event.eventResponseKey, jsend.error('account already imported'))
    } else if (d.map(x => x.name).includes(name)) {
      bridge.send(event.eventResponseKey, jsend.error('name already exists'))
    } else {
      d.push({
        // should only have private key directly in here if it is a Torus Wallet
        // Update 1.0.38 Torus wallets are also password protected so should never have private key directly
        name, pub, keystore, address, privateKey, torusType, torusName
      })

      await browser.storage.local.set({ ['wallets']: d })

      bridge.send('wallet.created', event.data)
      bridge.send(event.eventResponseKey, jsend.success(true))
    }
  })

  bridge.on('wallet.remove', async event => {
    // store the encrypted mnemonic into storage
    const walletIdx = event.data
    const { wallets } = await browser.storage.local.get(['wallets'])
    const d = wallets || []
    if (walletIdx < d.length) {
      d.splice(walletIdx, 1)
      await browser.storage.local.set({ ['wallets']: d })
      bridge.send(event.eventResponseKey, { data: { walletIdx } })
    } else {
      bridge.send(event.eventResponseKey, { error: 'Cannot find this wallet' })
    }
  })
  bridge.on('wallet.rename', async event => {
    // store the encrypted mnemonic into storage
    const { idx, name_new } = event.data
    const { wallets } = await browser.storage.local.get(['wallets'])
    const d = wallets || []
    // const idx = d.map(x => x.name).indexOf(name_old)
    if (idx >= 0 && idx < wallets.length) {
      d[idx].name = name_new
      await browser.storage.local.set({ ['wallets']: d })
      bridge.send(event.eventResponseKey, { data: true })
    } else {
      bridge.send(event.eventResponseKey, { error: 'Cannot find this wallet' })
    }
  })
  bridge.on('wallet.activity', async event => {
    // store the encrypted mnemonic into storage
    const { walletIdx, aryActivity } = event.data
    const { wallets } = await browser.storage.local.get(['wallets'])
    const walletActivity = (wallets || [])[walletIdx]?.activity || []

    // only put in the diffs
    const aryIds = walletActivity.map(a => a.id)
    let counter = 0
    for (const activity of aryActivity) {
      if (!aryIds.includes(activity.id)) {
        walletActivity.unshift(activity)
        counter++
      }
    }
    // This enforces that all activity is in asc order
    wallets[walletIdx].activity = walletActivity.sort((a, b) => b.id - a.id)

    await browser.storage.local.set({ ['wallets']: wallets })
    bridge.send(event.eventResponseKey, jsend.success({ counter }))
  })
  bridge.on('wallet.activity.erc20', async event => {
    // store the encrypted mnemonic into storage
    const { walletIdx, aryActivity } = event.data
    const { wallets } = await browser.storage.local.get(['wallets'])

    // put the whole thing as ERC20 is full query
    wallets[walletIdx].tokensErc20 = aryActivity

    await browser.storage.local.set({ ['wallets']: wallets })
    bridge.send(event.eventResponseKey, jsend.success({ counter }))
  })

  const onCloseListener = (tabId, removeInfo) => {
    const res = jsend.error({ code: 2, message: 'user closed confirm window', data: { tabId } })
    bridge.send('on.partisia.wallet.tab.relay', res)
  }
  bridge.on('on.confirm.connection.event', async event => {
    await bridge.send('on.partisia.wallet.tab.relay', event.data)
    bridge.send(event.eventResponseKey)
  })
  bridge.on('close.confirm.window', async event => {
    bridge.send('close.from.dom', { tabId: event.data.tabId })
  })
  bridge.on('disconnect.site', async event => {
    const { wallet_seed } = event.data
    const { confirmbox } = await browser.storage.local.get(['confirmbox'])
    const idx = confirmbox.findIndex(d => d.wallet_seed === wallet_seed)
    assert(idx !== -1)
    confirmbox.splice(idx, 1)
    await browser.storage.local.set({ ['confirmbox']: confirmbox })
    bridge.send(event.eventResponseKey, {})
  })

  bridge.on('set.wallet.connect', async event => {
    const { confirmbox } = await browser.storage.local.get(['confirmbox'])
    const idxBox = confirmbox.findIndex((d) => d.tabId === event.data.tabId && d.winType === 'connection')
    assert(idxBox !== -1)
    assert(!!!confirmbox[idxBox].onConnect, 'already connected')
    confirmbox[idxBox].onConnect = { ...event.data, time: Date.now() }
    await browser.storage.local.set({ ['confirmbox']: confirmbox })
    bridge.send(event.eventResponseKey, {})
  })
  bridge.on('open.popup.window', async event => {
    try {
      const tabId = await windowPopup()
      bridge.send(event.eventResponseKey, jsend.success({ tabId }))
    } catch (error) {
      bridge.send(event.eventResponseKey, jsend.error(error.message))
    }
  })
  bridge.on('open.confirm.window', async event => {
    try {
      // const activeTab = await browser.tabs.getCurrent()
      const tabs = await browser.tabs.query({ active: true, currentWindow: true })
      assert(tabs.length === 1)
      const tab = tabs[0]
      const winType = event.data.windowType

      let url
      if (winType === 'connection') {
        url = `www/index.html#/confirm-connection`
      } else if (winType === 'sign') {
        url = `www/index.html#/confirm-sign`
      } else if (winType === 'private_key') {
        url = `www/index.html#/confirm-key`
      }
      assert(url, 'invalid window type')

      // open the popup window and get tabId
      const createWindow = async () => {
        const winProps = await windowedProps()
        let confirmWin
        const payload = {
          ...winProps,
          focused: true,
          tabId: tab.id,
          url: browser.runtime.getURL(url),
        }

        // tabId fails for mozilla so try again without it
        try {
          confirmWin = await browser.windows.create(payload)
        } catch (error) {
          console.warn('tabId fails for this browser version so trying again without it')
          payload.tabId = undefined
          confirmWin = await browser.windows.create(payload)
        }
        const tabId = confirmWin?.tabs[0]?.id
        assert(tabId)
        // this creates a listener for when the tab closes
        browser.tabs.onRemoved.addListener(onCloseListener)
        return tabId
      }

      if (winType === 'connection') {
        assert(event.data.msgId, 'invalid params')
        assert(event.data.xpub, 'invalid params')
        const { confirmbox } = await browser.storage.local.get(['confirmbox'])
        const { permissions, dappName, chainId } = event.data.payload
        assert(dappName && chainId && permissions, 'invalid params')
        assert(permissions.length <= 2, 'invalid params')
        for (const p of permissions.filter((val, idx, ary) => (ary.indexOf(val) === idx))) {
          assert(['sign', 'private_key'].includes(p), 'invalid params')
        }

        // Now it is valid so open the window
        const tabId = await createWindow()

        const dapp = { permissions, dappName, chainId }
        const seed = crypto.randomBytes(32)
        const kp = partisiaCrypto.wallet.seedToKeyPair(seed)
        const d = confirmbox || []
        d.unshift({
          time: Date.now(),
          tabId,
          msgId: event.data.msgId,
          xpub: event.data.xpub,
          hd_idx: 0,
          aryPayloadHashes: [],
          winType,
          dapp,
          origin: new URL(tab.url).host,
          favIconUrl: tab.favIconUrl,
          wallet_seed: seed.toString('hex'),
        })
        // dont let d grow above 500
        while (d.length > 500) { d.pop() }
        await browser.storage.local.set({ ['confirmbox']: d })
        bridge.send(event.eventResponseKey, jsend.success({ tabId, box: kp.publicKey.toString('hex') }))


      } else if (['sign', 'private_key'].includes(winType)) {
        const { payload, windowType, connId } = event.data
        assert(typeof payload === 'string', 'invalid params')
        assert(typeof windowType === 'string' && windowType.length > 0, 'invalid params')
        assert(typeof connId === 'string' && connId.length > 0, 'invalid params')
        const { confirmbox } = await browser.storage.local.get(['confirmbox'])

        const idx = confirmbox.findIndex((d) => d.msgId === connId && d.winType === 'connection')

        assert(idx !== -1, 'Cannot Find Connection. Please logout and back in.')
        const conn = confirmbox[idx]
        assert(conn.dapp.permissions.includes(winType), 'you do not have permission for signing')

        // store a hash of the payload so it cannot get replayed
        // the ecies will have different encrypted payloads even if it is the same dq-card__section q-card__section--vert item-centereserialized payload so if it is the same it is a replay attack
        const hashPayload = partisiaCrypto.utils_buffer.createHashSha256(Buffer.from(payload, 'hex')).toString('hex')
        if (winType !== 'private_key') {
          assert(!conn.aryPayloadHashes.includes(hashPayload), 'Cannot replay same payload more than once')
        }

        conn.aryPayloadHashes.push(hashPayload)

        // increment the hd_idx so that nothing can get replayed
        conn.hd_idx += 1
        await browser.storage.local.set({ ['confirmbox']: confirmbox })

        const kp = partisiaCrypto.wallet.seedToKeyPair(conn.wallet_seed)

        let dapp
        if (winType === 'private_key') {
          assert(payload.length === 0)
          dapp = {}
        } else if (winType === 'sign') {
          assert(payload.length > 0)
          const bufPayload = partisiaCrypto.wallet.decryptMessage(kp.privateKey, payload)
          dapp = bson.deserialize(bufPayload, { promoteBuffers: true })
          assert(['utf8', 'hex', 'hex_payload'].includes(dapp.payloadType), 'invalid payload type')
        }

        // Now it is valid so open the window
        const tabId = await createWindow()

        // save to storage
        const { confirmsign } = await browser.storage.local.get(['confirmsign'])
        const d = confirmsign || []
        d.unshift({
          time: Date.now(),
          tabId,
          winType: windowType,
          dappPayloadDecrypted: {
            payload: dapp.payload,
            payloadType: dapp.payloadType,
            dontBroadcast: dapp.dontBroadcast,
          },
          conn,
        })
        while (d.length > 20) { d.pop() }
        await browser.storage.local.set({ ['confirmsign']: d })
        bridge.send(event.eventResponseKey, jsend.success({ tabId }))
      } else {
        throw new Error('winType not handled properly')
      }

    } catch (error) {
      bridge.send(event.eventResponseKey, jsend.error({ code: 4, message: error.message }))
      // if (error.message === 'invalid params') {
      // } else {
      //   bridge.send(event.eventResponseKey, jsend.error({ code: 1, message: 'Unable to Open Popup Window' }))
      // }
    }
  })
  // https://developer.chrome.com/docs/extensions/mv2/messaging/
  // browser.runtime.onConnect.addListener((port) => {
  // });

  /* for one time message */
  // chrome.runtime.onMessage.addListener(async (request, sender, sendResponse) => {
  //   console.log('message', { request, sender })
  //   alert(JSON.stringify(request))
  // });
  /*
  // EXAMPLES
  // Listen to a message from the client
  bridge.on('test', d => {
    console.log(d)
  })

  // Send a message to the client based on something happening.
  chrome.tabs.onCreated.addListener(tab => {
    bridge.send('browserTabCreated', { tab })
  })

  // Send a message to the client based on something happening.
  chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if (changeInfo.url) {
      bridge.send('browserTabUpdated', { tab, changeInfo })
    }
  })
   */
}
//
