import assert from 'assert'
import jsend from 'jsend'
import { isValidEventResponseKey } from './dom/valid-event'

// Hooks added here have a bridge allowing communication between the Web Page and the BEX Content Script.
// More info: https://quasar.dev/quasar-cli/developing-browser-extensions/dom-hooks

export default function attachDomHooks(bridge) {
  const mapPartisiaWalletTabEvent = new Map()

  window.__onPartisiaConfirmWin = async (payload, evt) => {
    try {
      if (process.env.DEV) console.log('payload', JSON.stringify(payload, null, 2))
      assert(evt.result === false && Object.keys(evt).length === 1)
      assert(['connection', 'sign', 'private_key'].includes(payload?.windowType))
      assert(payload.hasOwnProperty('payload'))

      if (payload.windowType === 'connection') {
        assert(payload.hasOwnProperty('xpub'))
        assert(payload.hasOwnProperty('msgId'))
        assert(Buffer.from(payload.msgId, 'hex').length === 33)
        const p = payload.payload
        assert(typeof p.chainId === 'string')
        assert(typeof p.dappName === 'string')
        assert(Array.isArray(p.permissions))
        for (const permission of p.permissions) {
          assert(['sign', 'private_key'].includes(permission))
        }
      } else if (payload.windowType === 'sign') {
        assert(payload.hasOwnProperty('connId'))
        const p = payload.payload
        assert(typeof p === 'string')
        assert(Buffer.from(p))
      } else if (payload.windowType === 'private_key') {
        assert(payload.hasOwnProperty('connId'))
        const p = payload.payload
        assert(typeof p === 'string' && p === '')
      } else {
        throw new Error('invalid winType implementation')
      }
      const res = await bridge.send('on.confirm.window', payload)
      evt.result = res.data
    } catch (error) {
      console.error(error.message)
      evt.result = jsend.error(error.message)
    }
  }
  window.__onPartisiaWalletTabEvent = async (tabId, evt) => {
    try {
      assert(typeof tabId === 'number')
      assert(evt.result === false && Object.keys(evt).length === 1)

      // Only allow one
      if (!mapPartisiaWalletTabEvent.has(tabId)) {
        mapPartisiaWalletTabEvent.set(tabId, evt)
      }
    } catch (error) {
      console.error(error.message)
      evt.result = jsend.error(error.message)
    }
  }

  bridge.on('on.dom.relay.tab', event => {
    // validate the event.eventResponseKey to make sure it only travels back to the content-hook
    assert(isValidEventResponseKey(event.eventResponseKey, 'on.dom.relay.tab') === true)

    // get the tabId from mapPartisiaWalletTabEvent and remove it
    const tabId = event.data.data.tabId
    if (mapPartisiaWalletTabEvent.has(tabId)) {
      const evt = mapPartisiaWalletTabEvent.get(tabId)
      evt.result = event.data
      mapPartisiaWalletTabEvent.delete(tabId)
    }

    // send back to the content-hook that this happened
    bridge.send(event.eventResponseKey)
  })

}
